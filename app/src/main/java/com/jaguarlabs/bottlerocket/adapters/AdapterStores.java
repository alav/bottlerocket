package com.jaguarlabs.bottlerocket.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaguarlabs.bottlerocket.R;
import com.jaguarlabs.bottlerocket.res.model.ModelDataResponse;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

/**
 * This adapter is used to populate and provide access to the
 * data items with the {@link android.widget.ListView}
 * used to show all stores in the main screen.
 */
public class AdapterStores extends BaseAdapter {

    //store selected
    private ModelDataResponse stores;

    private LayoutInflater inflater;
    private Context context;

    //image loader
    private ImageLoader imageLoader;
    private DisplayImageOptions defaultOptions;

    //listeners
    private OnStoreClickListener listener;
    private OnShowDialogFragmentFromAdapter listenerDialog;

    private LruCache<String, Bitmap> mMemoryCache;

    //cache booleans
    private boolean cacheNotAvailable = false;
    private boolean showCacheDialog;

    //constants
    public static final String IMAGE = "image";
    public static final String STORE_NAME = "storeName";

    public AdapterStores(Context context, ModelDataResponse stores, boolean showCacheDialog){
        this.context = context;
        this.stores = stores;

        this.inflater = LayoutInflater.from(context);
        this.imageLoader = ImageLoader.getInstance();

        this.showCacheDialog = showCacheDialog;

        initCache();
    }

    @Override
    public int getCount() {
        return stores.getStores().size();
    }

    @Override
    public Object getItem(int position) {
        return stores.getStores().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;
        ModelDataResponse.Store store = stores.getStores().get(position);

        if (convertView == null){
            convertView = inflater.inflate(R.layout.row_store, null);

            viewHolder = new ViewHolder();

            viewHolder.tvStoreName = (TextView) convertView.findViewById(R.id.tvStoreName);
            viewHolder.tvStorePhone = (TextView) convertView.findViewById(R.id.tvStorePhone);
            viewHolder.tvStoreAddress = (TextView) convertView.findViewById(R.id.tvStoreAddress);
            viewHolder.ivStoreLogo = (ImageView) convertView.findViewById(R.id.ivStoreLogo);
            viewHolder.ivStoreLogo.setMaxHeight(150);
            viewHolder.ivStoreLogo.setMaxWidth(250);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvStoreName.setText(store.getName());
        viewHolder.tvStorePhone.setText(store.getPhone());
        viewHolder.tvStoreAddress.setText(store.getAddress());

        //load image either from cache or internet
        imageLoader.loadImage(
                store.getStoreLogoURL(),
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        addBitmapToMemoryCache(imageUri + position, loadedImage);
                        viewHolder.ivStoreLogo.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                        Bitmap bitmapFromCache = getBitmapFromMemCache(imageUri + position);
                        if (bitmapFromCache != null) {
                            viewHolder.ivStoreLogo.setImageBitmap(bitmapFromCache);
                        } else {
                            //show cache dialog only once
                            if (!cacheNotAvailable) {
                                cacheNotAvailable = true;
                                if (showCacheDialog)
                                    listenerDialog.onShowDialogFragmentFromAdapter();
                            }
                        }

                        /*
                        switch (failReason.getType()){
                            case IO_ERROR:
                                break;
                            case DECODING_ERROR:
                                break;
                            case NETWORK_DENIED:

                                break;
                            case OUT_OF_MEMORY:
                                break;
                            case UNKNOWN:
                                break;
                        }
                        */
                    }
                }
        );

        //set transition name for
        //shared elements transitions
        //if available
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewHolder.ivStoreLogo.setTransitionName(IMAGE + position);
            viewHolder.tvStoreName.setTransitionName(STORE_NAME + position);
        }

        //set listener
        convertView.setOnClickListener(new View.OnClickListener() {

            private ModelDataResponse.Store store;
            private ImageView imageView;
            private String transitionName;
            private int position;
            private TextView textViewStoreName;

            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onStoreClick(store, imageView, transitionName, position, textViewStoreName);
                }
            }

            public View.OnClickListener setSelectedStore(ModelDataResponse.Store store, ImageView imageView, int position,
                                                         TextView textViewStoreName) {
                this.store = store;
                this.imageView = imageView;
                this.transitionName = IMAGE + position;
                this.position = position;
                this.textViewStoreName = textViewStoreName;
                return this;
            }
        }.setSelectedStore(store, viewHolder.ivStoreLogo, position, viewHolder.tvStoreName));

        return convertView;
    }

    /**
     * ViewHolder class to store each of the component views
     * inside the tag field of the layout
     */
    private class ViewHolder{
        private TextView tvStoreName;
        private TextView tvStorePhone;
        private TextView tvStoreAddress;
        public ImageView ivStoreLogo;
    }

    /**
     * init own cache (different than image loader)
     */
    private void initCache(){
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    /**
     * adds bitmap to memory cache
     * @param key String
     * @param bitmap Bitmap
     */
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    /**
     * retrieves bitmap from cache
     * @param key
     * @return bitmap object
     */
    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    /**
     * set listener for interface
     * see {@link com.jaguarlabs.bottlerocket.adapters.AdapterStores.OnStoreClickListener}
     * @param listener
     */
    public void setOnStoreClickListener(OnStoreClickListener listener){
        this.listener = listener;
    }

    /**
     * interface that manages when a store is selected
     */
    public interface OnStoreClickListener{
        public void onStoreClick(ModelDataResponse.Store store, ImageView imageView, String transitionName, int position, TextView textViewStoreName);
    }

    /**
     * set listener for interface
     * see {@link com.jaguarlabs.bottlerocket.adapters.AdapterStores.OnShowDialogFragmentFromAdapter}
     * @param listener
     */
    public void setOnOnShowDialogFragmentFromAdapterListener(OnShowDialogFragmentFromAdapter listener){
        this.listenerDialog = listener;
    }

    /**
     * interface that manages when a dialog has to be shown
     */
    public interface OnShowDialogFragmentFromAdapter{
        public void onShowDialogFragmentFromAdapter();
    }
}
