package com.jaguarlabs.bottlerocket.components;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.jaguarlabs.bottlerocket.res.model.ModelDataResponse;

/**
 * This class will be in charge of saving json info retrieved
 * from internet, so that if an internet conection is not available, it'll
 * still be able to show info info of stores.
 */
public class Preferences {

    //constants
    private static final String PREFERENCES_FILE = "STORES_DATA";
    private static final String KEY_JSON = "KEY_JSON";

    //preference objects
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    //context
    private Context context;

    public Preferences(Context context){
        this.context = context;

         preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
         editor = preferences.edit();
    }

    /**
     * saves json info as a string
     * @param modelDataResponse
     */
    public void saveJsonInfo(ModelDataResponse modelDataResponse){
        editor.putString(KEY_JSON, new Gson().toJson(modelDataResponse));
        editor.commit();
    }

    /**
     * retrieves json info and convert it to ModelDataResponse object
     * @return ModelDataResponse object
     */
    public ModelDataResponse getJsonInfo(){
        String modelDataResponseJson = preferences.getString(KEY_JSON, null);
        if (modelDataResponseJson != null) {
            ModelDataResponse modelDataResponse = new Gson().fromJson(modelDataResponseJson, ModelDataResponse.class);
            return modelDataResponse;
        }
        return null;
    }
}
