package com.jaguarlabs.bottlerocket.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jaguarlabs.bottlerocket.R;
import com.jaguarlabs.bottlerocket.activities.MainActivity;
import com.jaguarlabs.bottlerocket.res.model.ModelDataResponse;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Locale;

/**
 * Class used to show store details
 */
public class FragmentStoreDetail extends BaseSpiceFragment {
    private ModelDataResponse.Store store;

    private View layout;

    private ImageLoader imageLoader;

    //view's components
    private ImageView ivStoreLogo;
    private ImageView ivLocationGM;
    private TextView tvStoreName;
    private TextView tvStoreAddress;
    private TextView tvCityState;
    private TextView tvZipCode;
    private TextView tvStoreID;
    private TextView tvStorePhone;
    private TextView tvStoreLatitude;
    private TextView tvStoreLongitude;
    private ImageView ivRoundedLocation;
    private ImageView ivGoBack;

    //constants
    public static final String STATE_STORE = "storeInfo";
    public static final String GEO = "geo:";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.imageLoader = ImageLoader.getInstance();

        if (savedInstanceState != null){
            //Go back to store list when in landscape
            store = new Gson().fromJson( savedInstanceState.getString(STATE_STORE), ModelDataResponse.Store.class );
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //instantiate components
        layout = inflater.inflate(R.layout.fragment_store_detail, null);

        ivStoreLogo = (ImageView) layout.findViewById(R.id.ivStoreLogoD);

        tvStoreName = (TextView) layout.findViewById(R.id.tvStoreName);
        tvStoreAddress = (TextView) layout.findViewById(R.id.tvStoreAddress);
        tvCityState = (TextView) layout.findViewById(R.id.tvCityState);
        tvZipCode = (TextView) layout.findViewById(R.id.tvZipCode);
        tvStoreID = (TextView) layout.findViewById(R.id.tvStoreID);
        tvStorePhone = (TextView) layout.findViewById(R.id.tvStorePhone);
        tvStoreLatitude = (TextView) layout.findViewById(R.id.tvStoreLatitude);
        tvStoreLongitude = (TextView) layout.findViewById(R.id.tvStoreLongitude);

        ivRoundedLocation = (ImageView) layout.findViewById(R.id.ivRoundedLocation);
        ivGoBack = (ImageView) layout.findViewById(R.id.ivGoback);

        //set transition name if available
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ivStoreLogo.setTransitionName(getString(R.string.image) + ((MainActivity) getActivity()).getPosition());
            tvStoreName.setTransitionName(getString(R.string.store_name) + ((MainActivity) getActivity()).getPosition());
        }

        setListeners();

        return layout;
    }

    /**
     * sets listeners
     */
    private void setListeners(){
        ivRoundedLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String uri = String.format(Locale.ENGLISH, GEO + store.getLatitude() + "," + store.getLongitude());
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    getActivity().startActivity(intent);
                } catch (ActivityNotFoundException e){
                    Toast.makeText(getActivity(), R.string.error_message_no_app_found, Toast.LENGTH_SHORT).show();
                }
            }
        });

        ivGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        setComponents();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putString(STATE_STORE, new Gson().toJson(store));

        super.onSaveInstanceState(outState);
    }

    private void setComponents(){

        //load image either from cache or internet
        imageLoader.loadImage(
                store.getStoreLogoURL(),
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        //addBitmapToMemoryCache(imageUri + position, loadedImage);
                        ivStoreLogo.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        //nothing here...
                    }
                }
        );

        //set view's components
        tvStoreName.setText(store.getName());
        tvStoreAddress.setText(store.getAddress());
        tvCityState.setText(store.getCity() + ", " + store.getState());
        tvZipCode.setText(store.getZipCode());
        tvStoreID.setText(store.getStoreID());
        tvStorePhone.setText(store.getPhone());
        tvStoreLatitude.setText(store.getLatitude());
        tvStoreLongitude.setText(store.getLongitude());
    }

    /**
     * set selected store
     * @param store
     */
    public void setStore(ModelDataResponse.Store store){
        this.store = store;
    }
}
