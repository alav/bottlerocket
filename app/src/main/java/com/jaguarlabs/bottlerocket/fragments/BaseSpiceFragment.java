package com.jaguarlabs.bottlerocket.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import com.jaguarlabs.bottlerocket.res.RestService;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.octo.android.robospice.SpiceManager;

/**
 * This class is the base class of all fragments of the project.
 */
public class BaseSpiceFragment extends Fragment {

    private SpiceManager spiceManager = new SpiceManager(RestService.class);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        spiceManager.start(getActivity());
        super.onStart();
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    /**
     * retrieve spice manager instance
     * @return
     */
    public SpiceManager getSpiceManager(){
        return spiceManager;
    }
}
