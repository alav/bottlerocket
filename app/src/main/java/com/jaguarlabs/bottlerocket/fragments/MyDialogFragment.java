package com.jaguarlabs.bottlerocket.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.jaguarlabs.bottlerocket.R;

/**
 * Class used to show dialog either about internet or
 * cache problem
 */
public class MyDialogFragment extends android.app.DialogFragment {

    private OnDialogClickListener listener;
    private int code;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //if code is equal to 1 or 2, show internet problem dialog
        //when code is equal to 1, add listener for negative button to close app
        //when code is equal to 2, do not close app when clicking negative button
        //if code is different than 1 or 2, show cache problem dialog
        if (code == 1 || code == 2) {
            return new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.error_title_network)
                    .setMessage(R.string.error_message_network)
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (code == 1) {
                                android.os.Process.killProcess(android.os.Process.myPid());
                            }
                        }
                    })
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (listener != null) {
                                listener.onDialogClick();
                            }
                        }
                    })
                    .create();
        }
        else {
            return new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.error_title_cache)
                    .setMessage(R.string.error_message_cache)
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //no custom behaviour
                        }
                    })
                    .create();
        }
    }

    /**
     * set code
     * @param code
     */
    public void setCode(int code){
        this.code = code;
    }

    /**
     * set listener
     * @param listener
     */
    public void setOnDialogClickListener(OnDialogClickListener listener){
        this.listener = listener;
    }

    /**
     * define interface
     */
    public interface OnDialogClickListener{
        public void onDialogClick();
    }

}
