package com.jaguarlabs.bottlerocket.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.jaguarlabs.bottlerocket.adapters.AdapterStores;
import com.jaguarlabs.bottlerocket.R;
import com.jaguarlabs.bottlerocket.components.Preferences;
import com.jaguarlabs.bottlerocket.res.SpiceHelper;
import com.jaguarlabs.bottlerocket.res.model.ModelDataResponse;
import com.jaguarlabs.bottlerocket.res.request.DataRequest;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestProgress;

import java.util.ArrayList;

/**
 * Class in charge of showing list of stores
 */
public class FragmentStoreList extends BaseSpiceFragment {

    private View layout;
    private ListView lvStores;
    private AdapterStores adapterStores;
    private ModelDataResponse stores;
    private ModelDataResponse foundStores;

    private SwipeRefreshLayout swipeRefreshLayout;

    private SpiceManager spiceManager = getSpiceManager();
    private SpiceHelper<ModelDataResponse> spiceGetData;

    private AdapterStores.OnStoreClickListener listener;
    private OnShowDialogFragment listenerDialog;
    private AdapterStores.OnShowDialogFragmentFromAdapter listenerDialogFromAdapter;

    private FrameLayout progressBarContainer;

    private boolean dataFromCache = false;

    private EditText etSearch;
    private boolean showCacheDialog = true;

    public static final String GET_DATA = "GET_DATA";

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        //change screen name
        if (!hidden){
            getActivity().setTitle(R.string.title_stores);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragment_stores,null);

        //set screen name
        getActivity().setTitle(R.string.title_stores);

        //define ImageLoaderConfiguration options
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder)
                .showImageForEmptyUri(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(1000))
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .diskCacheSize(100 * 1024 * 1024)
                .build();

        ImageLoader.getInstance().init(config);

        //instantiate components
        swipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swiperefresh);
        etSearch = (EditText) layout.findViewById(R.id.etSearch);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initComponents();

        //set listeners
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                etSearch.setText("");
                DataRequest dataRequest = new DataRequest();
                spiceGetData.executeRequest(dataRequest);
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showCacheDialog = false;
                if (etSearch.length() > 0) {
                    foundStores.setStores(searchStores(s.toString()));
                    if (foundStores == null) {
                        foundStores = new ModelDataResponse();
                    }
                    if (foundStores.getStores().size() > -1) {
                        fillListView(foundStores);
                    }

                } else {
                    fillListView(stores);
                }
            }
        });
        return layout;
    }

    /**
     * search for stores which contain text given on their name
     * @param args
     * @return ArrayList<ModelDataResponse.Store>
     */
    private ArrayList<ModelDataResponse.Store> searchStores(String args){
        ArrayList<ModelDataResponse.Store> filteredStores = new ArrayList<>();
        String lowerCaseGivenString = "";

        //change given string to lowercase
        for (int i = 0; i < args.length(); i++){
            if (args.charAt(i) >= 65 && args.charAt(i) <= 90 ) {
                lowerCaseGivenString += (char)(args.charAt(i) + 32);
            }
            else {
                lowerCaseGivenString += args.charAt(i);
            }
        }

        //compare with every store name
        for (int i = 0; i < stores.getStores().size(); i++){
            String storeNameLowerCase = "";
            String storeName = stores.getStores().get(i).getName();

            //change store name to lowercase
            for (int j = 0; j < storeName.length(); j++){
                if (storeName.charAt(j) >= 65 && storeName.charAt(j) <= 90 ) {
                    storeNameLowerCase += (char)(storeName.charAt(j) + 32);
                }
                else {
                    storeNameLowerCase += storeName.charAt(j);
                }
            }

            //if store name contains text given, add to filteredStores ArrayList
            //which is going to be returned
            if ( storeNameLowerCase.contains(lowerCaseGivenString) ){
                filteredStores.add(stores.getStores().get(i));
            }
        }
        return filteredStores;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //instantiate spice helper
        spiceGetData = new SpiceHelper<ModelDataResponse>(spiceManager, GET_DATA, ModelDataResponse.class) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                swipeRefreshLayout.setRefreshing(false);
                dismissProgressBar();
                getDataFromPreference();
            }

            @Override
            protected void onSuccess(ModelDataResponse modelDataResponse) {
                super.onSuccess(modelDataResponse);
                swipeRefreshLayout.setRefreshing(false);
                onGettingInfoSuccess(modelDataResponse);
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                super.onProgressUpdate(progress);
            }
        };

        //execute request
        DataRequest dataRequest = new DataRequest();
        spiceGetData.executeRequest(dataRequest);
    }

    /**
     * if succeed from getting info from internet, this
     * method gets called
     * @param modelDataResponse
     */
    private void onGettingInfoSuccess(ModelDataResponse modelDataResponse){
        stores = modelDataResponse;
        if (!dataFromCache) {
            Preferences preferences = new Preferences(getActivity());
            preferences.saveJsonInfo(stores);
        }
        fillListView(stores);
    }

    /**
     * if internet is not available, try to get previous data from
     * preference file (xml), if is not possible, show dialog
     */
    public void getDataFromPreference(){
        Preferences preferencesCache = new Preferences(getActivity());
        ModelDataResponse modelDataResponse = preferencesCache.getJsonInfo();
        if (modelDataResponse != null){
            showDialogFragment(2);
            dataFromCache = true;
            onGettingInfoSuccess(modelDataResponse);
        }
        else {
            showDialogFragment(1);
        }
    }

    /**
     * execute request
     */
    public void executeGetDataRequest(){
        showProgressBar();
        DataRequest dataRequest = new DataRequest();
        spiceGetData.executeRequest(dataRequest);
    }

    /**
     * shows dialog
     * @param code
     */
    private void showDialogFragment(int code) {
        if (listenerDialog != null){
            listenerDialog.onShowDialog(code);
        }
    }

    /**
     * init components
     */
    private void initComponents(){

        lvStores = (ListView) layout.findViewById(R.id.lvStores);

        progressBarContainer = (FrameLayout) layout.findViewById(R.id.ProgressBar_Container);

        stores = new ModelDataResponse();
        foundStores = new ModelDataResponse();
    }

    /**
     * fills list view and set listener for interfaces of adapter
     * @param stores
     */
    private void fillListView(ModelDataResponse stores){
        adapterStores = new AdapterStores(getActivity(),stores, showCacheDialog);

        if (listener != null){
            adapterStores.setOnStoreClickListener(listener);
        }
        if (listenerDialogFromAdapter != null){
            adapterStores.setOnOnShowDialogFragmentFromAdapterListener(listenerDialogFromAdapter);
        }

        dismissProgressBar();

        lvStores.setAdapter(adapterStores);
        lvStores.setDividerHeight(10);
    }

    /**
     * dismisses progress bar
     */
    private void dismissProgressBar(){
        progressBarContainer.setVisibility(View.GONE);
    }

    /**
     * shows progress bar
     */
    private void showProgressBar(){
        progressBarContainer.setVisibility(View.VISIBLE);
    }

    /**
     * set listener
     * when a store is selected
     * @param listener
     */
    public void setOnStoreClickListener(AdapterStores.OnStoreClickListener listener){
        this.listener = listener;
    }

    /**
     * set listener
     * when a dialog has to be shown
     * @param listener
     */
    public void setOnShowDialogFragment(OnShowDialogFragment listener){
        this.listenerDialog = listener;
    }

    /**
     * set listener
     * when a dialog from adapter has to be shown
     * @param listener
     */
    public void setOnShowDialogFromAdapter(AdapterStores.OnShowDialogFragmentFromAdapter listener){
        this.listenerDialogFromAdapter = listener;
    }

    /**
     * define dialog interface
     */
    public interface OnShowDialogFragment{
        public void onShowDialog(int code);
    }
}