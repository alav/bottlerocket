package com.jaguarlabs.bottlerocket.res;

import android.app.Application;
import android.util.Log;

import com.jaguarlabs.bottlerocket.res.model.ModelDataResponse;
import com.jaguarlabs.bottlerocket.res.persister.GsonObjectPersister;
import com.jaguarlabs.bottlerocket.res.webService.DataService;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import java.io.IOException;
import java.net.HttpURLConnection;

import retrofit.RestAdapter;
import retrofit.client.Request;
import retrofit.client.UrlConnectionClient;

/**
 * Created by jordan on 23/02/16.
 */
public class RestService extends RetrofitGsonSpiceService {

    public final static String BASE_URL = "http://sandbox.bottlerocketapps.com/BR_Android_CodingExam_2015_Server";

    @Override
    public void onCreate() {
        super.onCreate();

        addRetrofitInterface(DataService.class);
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();

        GsonObjectPersister<ModelDataResponse> persisterData = new GsonObjectPersister<ModelDataResponse>(application, ModelDataResponse.class);

        cacheManager.addPersister(persisterData);

        return cacheManager;
    }

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RestAdapter.Builder builder = super.createRestAdapterBuilder();

        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setLog(new RestAdapter.Log() {
            @Override
            public void log(String message) {
                Log.i(null, message);
            }
        });
        builder.setClient(new UrlConnectionClient(){
            @Override
            protected HttpURLConnection openConnection(Request request) throws IOException {
                HttpURLConnection connection = super.openConnection(request);
                connection.setConnectTimeout(30*1000);
                connection.setReadTimeout(30*1000);

                return connection;
            }
        });

        return builder;
    }

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }
}
