package com.jaguarlabs.bottlerocket.res.webService;

import com.jaguarlabs.bottlerocket.res.model.ModelDataResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by jordan on 23/02/16.
 */
public interface DataService {

    @POST("/stores.json")
    public ModelDataResponse requestData();
}
