package com.jaguarlabs.bottlerocket.res.request;

import android.util.Log;

import com.jaguarlabs.bottlerocket.res.model.ModelDataResponse;
import com.jaguarlabs.bottlerocket.res.webService.DataService;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by jordan on 23/02/16.
 */
public class DataRequest extends RetrofitSpiceRequest<ModelDataResponse, DataService> {

    public DataRequest(){
        super(ModelDataResponse.class, DataService.class);
    }

    @Override
    public ModelDataResponse loadDataFromNetwork() throws Exception {
        return getService().requestData();
    }
}
