package com.jaguarlabs.bottlerocket.res.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by jordan on 23/02/16.
 */
public class ModelDataResponse {

    private ArrayList<Store> stores;

    public ModelDataResponse(){
        stores = new ArrayList<>();
    }

    public ArrayList<Store> getStores(){
        return stores;
    }

    public void setStores(ArrayList<Store> stores){
        this.stores = stores;
    }

    public class Store{
        private String address;
        private String city;
        private String name;
        private String latitude;
        private String zipcode;
        private String storeLogoURL;
        private String phone;
        private String longitude;
        private String storeID;
        private String state;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getZipCode() {
            return zipcode;
        }

        public void setZipCode(String zipCode) {
            this.zipcode = zipCode;
        }

        public String getStoreLogoURL() {
            return storeLogoURL;
        }

        public void setStoreLogoURL(String storeLogoURL) {
            this.storeLogoURL = storeLogoURL;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStoreID() {
            return storeID;
        }

        public void setStoreID(String storeID) {
            this.storeID = storeID;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }

}
