package com.jaguarlabs.bottlerocket.res;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by jordan on 24/02/16.
 */
public abstract class SpiceHelper<CLASS_RESPONSE> {

    private SpiceManager spiceManager;
    private String cacheKey;
    private boolean isRequestPending;
    private Class responseClass;

    public SpiceHelper(SpiceManager spiceManager, String cacheKey, Class responseClass){
        this.cacheKey = cacheKey;
        this.spiceManager = spiceManager;
        this.responseClass = responseClass;
        isRequestPending = false;
    }

    private void clearCacheRequest(){
        try {
            Future<?> future = spiceManager.removeAllDataFromCache();
            if (future != null){
                future.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    protected void onFail(SpiceException exception){
        clearCacheRequest();
        isRequestPending = false;
    }

    protected void onSuccess(CLASS_RESPONSE response){
        clearCacheRequest();
        isRequestPending = false;
    }

    protected void onProgressUpdate(RequestProgress progress){
    }

    public void executeRequest(SpiceRequest<CLASS_RESPONSE> request){
        isRequestPending = true;
        spiceManager.execute(request, cacheKey, DurationInMillis.ONE_DAY, new SpiceRequestListener());
    }

    public void executePendingRequest(){
        isRequestPending = true;
        spiceManager.addListenerIfPending(responseClass, cacheKey, new SpicePendingRequestListener());
    }

    public boolean isRequestPending() {
        return isRequestPending;
    }

    private class SpiceRequestListener implements RequestListener<CLASS_RESPONSE>, RequestProgressListener {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onFail(spiceException);
        }

        @Override
        public void onRequestSuccess(CLASS_RESPONSE response) {
            onSuccess(response);
        }

        @Override
        public void onRequestProgressUpdate(RequestProgress progress) {
            onProgressUpdate(progress);
        }
    }

    private class SpicePendingRequestListener implements PendingRequestListener<CLASS_RESPONSE> {

        @Override
        public void onRequestNotFound() {
            if (isRequestPending){
                spiceManager.getFromCache(responseClass, cacheKey, DurationInMillis.ONE_DAY, new SpiceRequestListener());
            }
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onFail(spiceException);
        }

        @Override
        public void onRequestSuccess(CLASS_RESPONSE response) {
            onSuccess(response);
        }
    }

}
