package com.jaguarlabs.bottlerocket.activities;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.transition.Explode;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaguarlabs.bottlerocket.adapters.AdapterStores;
import com.jaguarlabs.bottlerocket.fragments.BaseSpiceFragment;
import com.jaguarlabs.bottlerocket.transitions.DetailsTransition;
import com.jaguarlabs.bottlerocket.R;
import com.jaguarlabs.bottlerocket.fragments.FragmentStoreDetail;
import com.jaguarlabs.bottlerocket.fragments.FragmentStoreList;
import com.jaguarlabs.bottlerocket.fragments.MyDialogFragment;
import com.jaguarlabs.bottlerocket.res.model.ModelDataResponse;

/**
 * This is the main activity from where everything flows.
 * Manages transactions of fragments
 * and defines different fragment listeners
 */
public class MainActivity extends BaseActivity implements AdapterStores.OnStoreClickListener, MyDialogFragment.OnDialogClickListener, FragmentStoreList.OnShowDialogFragment, AdapterStores.OnShowDialogFragmentFromAdapter {

    //fragments
    private FragmentStoreList fragmentStoreList;
    private FragmentStoreDetail fragmentStoreDetail;
    private MyDialogFragment myDialogFragment;

    //position of selected store
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //instantiate and make transaction
        fragmentStoreList = new FragmentStoreList();
        fragmentStoreList.setOnStoreClickListener(this);
        fragmentStoreList.setOnShowDialogFragment(this);
        fragmentStoreList.setOnShowDialogFromAdapter(this);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragmentStoreList);
        transaction.show(fragmentStoreList);
        transaction.commit();
    }

    @Override
    public void onStoreClick(ModelDataResponse.Store store, ImageView imageView, String transitionName, int position, TextView textViewStoreName) {

        this.setPosition(position);

        //instantiate and make transaction
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        fragmentStoreDetail = new FragmentStoreDetail();
        fragmentStoreDetail.setStore(store);

        //set shared element transition if available
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragmentStoreDetail.setSharedElementEnterTransition(new DetailsTransition());
            fragmentStoreDetail.setEnterTransition(new Explode().setDuration(700));
            fragmentStoreDetail.setSharedElementReturnTransition(new DetailsTransition());
        }

        transaction.hide(fragmentStoreList);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            transaction.addSharedElement(imageView, transitionName);
            transaction.addSharedElement(textViewStoreName, getString(R.string.transaction_store_name) + position);
        }

        transaction.add(R.id.fragment_container, fragmentStoreDetail);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onDialogClick() {
        fragmentStoreList.executeGetDataRequest();
    }

    @Override
    public void onShowDialog(int code) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        myDialogFragment = new MyDialogFragment();
        myDialogFragment.setCode(code);
        myDialogFragment.setOnDialogClickListener(this);
        myDialogFragment.show(fragmentTransaction, getString(R.string.dialog));
    }

    /**
     * set position of selected store
     * @param position
     */
    public void setPosition(int position){
        this.position = position;
    }

    /**
     * retrieve position of selected store
     * @return integer position
     */
    public int getPosition(){
        return position;
    }

    @Override
    public void onShowDialogFragmentFromAdapter() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        myDialogFragment = new MyDialogFragment();
        myDialogFragment.setCode(3);
        myDialogFragment.setOnDialogClickListener(this);
        myDialogFragment.show(fragmentTransaction, getString(R.string.dialog));
    }

}
